import re
import numpy as np


def clean(full_name):
    full_name = ' '.join(full_name.split())
    full_name = re.sub('[^a-zA-Z\s]', '', full_name)
    return full_name.lower()

def create_feature_vector(cleaned_full_name):
    list_of_numbers = []
    
    for char in cleaned_full_name:
        if char.isspace(): #if char is a space
            list_of_numbers.append(0)
        else:
            list_of_numbers.append(ord(char) - 96)

    one_hot = np.zeros((50, 27), dtype=np.int8)
    one_hot[np.arange(len(list_of_numbers)), list_of_numbers] = 1
    return list(one_hot.flatten())
