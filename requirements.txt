Django==1.11
numpy==1.11.0
pandas==0.23.1
python-dateutil==2.8.1
pytz==2019.3
scikit-learn==0.20.0
scipy==0.17.0
six==1.13.0
