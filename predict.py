import numpy as np
from sklearn.externals import joblib
import data_preprocessing


def race(name):
    predicted_race = {'c': 'chinese', 'm': 'malay', 'i': 'indian',}
    clf = joblib.load('race_classification.pkl')
    vector = np.array(
        data_preprocessing.create_feature_vector(
            data_preprocessing.clean(name))).reshape(1, -1)
    label = clf.predict(vector)[0]
    return predicted_race[label]

print(race('eric Chang'))