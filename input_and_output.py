import numpy as np
import random
import pandas as pd

from sklearn import svm
from sklearn.externals import joblib

from data_preprocessing import clean, create_feature_vector


chinese = 'chinese.csv'
malay = 'malay.csv'
indian = 'indian.csv'

def sample_handling(csv, classification):
    samples = []
    df = pd.read_csv(csv).head(3000)
    names = df.name.tolist()
    for name in names:
        samples.append([create_feature_vector(clean(name)), classification])
    return samples

all_samples = []
all_samples += sample_handling(chinese, 'c')
all_samples += sample_handling(malay, 'm')
all_samples += sample_handling(indian, 'i')

random.shuffle(all_samples)
all_samples = np.array(all_samples)

test_size = 0.1
testing_size = int(test_size * len(all_samples))

train_x = np.array(list(all_samples[:,0][:-testing_size]))
train_y = all_samples[:,1][:-testing_size]
test_x = np.array(list(all_samples[:,0][-testing_size:]))
test_y = all_samples[:,1][-testing_size:]
print ('Shape:')
print ('train_x: ', train_x.shape)
print ('train_y: ', train_y.shape)
print ('test_x: ', test_x.shape)
print ('test_y: ', test_y.shape)

clf = svm.LinearSVC(C=1.0)

# pass training data to the classifier & train it
clf.fit(train_x, train_y)
joblib.dump(clf, 'race_classification.pkl')
print("Accurary of the model: ", clf.score(test_x, test_y))

example1 = np.array(create_feature_vector(clean('Iskandar'))).reshape(1, -1)
# output prediction or classes corresponded to the input vector
print (clf.predict(example1))
