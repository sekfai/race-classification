# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse


# def index(request):
#     return HttpResponse("Hello, world. You're at the polls index.")


def index(request):
    name = request.GET.get('name', '')

    if not name:
        return render(request, 'race_classification.html')
    
    import predict

    return render(request, 'race_classification.html', {
        'name': name,
        'race': predict.race(name),
    })
